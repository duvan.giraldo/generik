﻿using Microsoft.EntityFrameworkCore;
using user.infrastructure.Dao;

namespace user.infrastructure.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<UserDao> Users { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=UsersDB;Username=postgres;Password=postgres");
        }

        public AppDbContext(DbContextOptions options) : base(options) { }

        public AppDbContext()
        {

        }
    }
}
